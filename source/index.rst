AiThinker-FAQ
=================================


| AiThinker-FAQ 是由安信可官方推出的针对常见问题的总结。在线帮助我们的用户快速检索经常问到的问题，通过简单的解释获得解答。

==================  ==================  ==================
|使用说明|_          |开发环境|_         |应用方案|_
------------------  ------------------  ------------------
`使用说明`_          `开发环境`_         `应用方案`_
------------------  ------------------  ------------------
|软件使用|_          |硬件相关|_         |出厂固件|_
------------------  ------------------  ------------------
`软件使用`_          `硬件相关`_         `出厂固件`_
==================  ==================  ==================



.. |使用说明| image:: _static/instruction.png
.. _使用说明: docs/instruction/index.html

.. |开发环境| image:: _static/development-environment.png
.. _开发环境: docs/development-environment/index.html

.. |应用方案| image:: _static/application-solution.png
.. _应用方案: docs/application-solution/index.html

.. |软件使用| image:: _static/software-framework.png
.. _软件使用: docs/software-framework/index.html

.. |硬件相关| image:: _static/hardware-related.png
.. _硬件相关: docs/hardware-related/index.html

.. |出厂固件| image:: _static/test-verification.png
.. _出厂固件: docs/taobao/index.html



联系我们
~~~~~~~~~~~~~~
1. 样品购买： https://anxinke.taobao.com
2. 样品资料： https://docs.ai-thinker.com
3. 商务合作： 0755-29162996
4. 公司地址： 深圳市宝安区西乡固戍华丰智慧创新港C栋410


.. toctree::
   :hidden:
   :maxdepth: 2
   :glob:

   docs/instruction/index
   docs/development-environment/index
   docs/application-solution/index
   docs/software-framework/index
   docs/hardware-related/index
   docs/taobao/index
   