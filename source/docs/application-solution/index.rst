应用方案
=============

本节简单介绍我司模组的一些成熟方案，持续更新中...

天猫精灵/阿里飞燕平台
--------------

1、 AT透传指令接入阿里云物联网平台笔记（非天猫精灵语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：
https://aithinker.blog.csdn.net/article/details/104791452

2、安信可自研固件接入天猫精灵IOT平台控制单路插座的教程 （支持天猫精灵语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：

https://aithinker.blog.csdn.net/article/details/106059991

3、安信可自研固件指令接入天猫精灵IOT平台实现控制RGB冷暖灯的教程 （支持天猫精灵语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：
https://aithinker.blog.csdn.net/article/details/105970895

4、安信可ESP-12S对接代码教程（AT+MCU方式）：
https://aithinker.blog.csdn.net/article/details/105676625

5、安信可自研固件指令接入接入阿里飞燕 , 轻松天猫精灵语音控制单片机。 （天猫精灵语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：
https://aithinker.blog.csdn.net/article/details/105651819

京东小京鱼平台
--------------
补充中...

腾讯物联/微信小程序平台
--------------

1、对接腾讯物联开发平台 （不支持任何音响语音控制）
安信可ESP-12S对接代码教程（AT或SDK开发）：
https://github.com/Ai-Thinker-Open/Ai-Thinker-Open-qcloud-esp-wifi

微信小程序MQTT通过连接百度天工远程控制安信可ESP8266开发板； （不支持任何音响语音控制）
B站视频教程：
https://www.bilibili.com/video/BV1W7411o7cg

微信小程序源码：
https://github.com/Ai-Thinker-Open/WCMiniColorControl

设备端源码：
https://github.com/Ai-Thinker-Open/AiClouds3.0-Device/tree/master/Ai-examples/4.BaiDu-rgb-esp8266




亚马逊/微软IoT平台
--------------

1、AT对接亚马逊云平台（不支持任何音响语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：
https://aithinker.blog.csdn.net/article/details/104633166

2、AT透传指令接入微软IOT物联网平台 （不支持任何音响语音控制）
安信可ESP-12S对接代码教程（AT+MCU方式）：https://aithinker.blog.csdn.net/article/details/104836750
