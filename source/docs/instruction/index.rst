使用说明
=============

本节为 AiThinker-FAQ 的使用说明。“问题查找”旨在帮助您快速了解该网站的搜索方法及分类框架，节省问题查找时间。

搜索问题技巧
------------

目前可以归纳出以下 2 种搜索问题的技巧：

- 搜索关键词 
- 排除某个关键词

搜索关键词
~~~~~~~~~~

将问题中的关键词提取出来并搜索它们，此时搜索结果会得到最匹配的结果。


比如问题为：``ESP32 的 Bluetooth LE 吞吐量是多少？``

此时搜索：``ESP32``、``BLE`` 和 ``吞吐量`` 等关键字为宜。

排除某个关键词
~~~~~~~~~~~~~~

在搜索内容中添加一个标识符 ``-``, 格式为： ``关键词  -排除关键词``。此时搜索结果不会出现有排除关键词的结果。

比如搜索：``ESP32 -ble``。任何搜索结果中有 ``ble`` 的结果将会被过滤。


