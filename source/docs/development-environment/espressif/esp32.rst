ESP32系列
====


简介
~~~~~~~~

　　ESP32系列模组是深圳市安信可科技有限公司开发的一系列基于乐鑫ESP32的低功耗UART-WiFi芯片模组，可以方便地进行二次开发，接入云端服务，实现手机3/4G全球随时随地的控制，加速产品原型设计。

-  `ESP32-S模组样品购买 戳我 <https://item.taobao.com/item.htm?spm=a1z10.5-c-s.w4002-16491566042.11.1de07cd72gBZr4&id=548905088891>`__
-  `ESP-A1S模组样品购买 戳我 <https://item.taobao.com/item.htm?spm=a1z10.5-c-s.w4002-16491566042.23.1de07cd72gBZr4&id=574509789383>`__


如何搭建开发环境
~~~~~~~~~~~~
    ESP32的开发环境支持Linux、Windows和Mac系统，
    视频教学：https://www.bilibili.com/video/BV1Ke411s7Go