ESP32-S2系列
====


简介
~~~~~~~~

　　ESP32-S2系列模组的芯片ESP32-S2芯片搭载 Xtensa® 32 位 LX7 单核处理器，工作频率高达 240 MHz。芯片支持二次开发，无需使用其它微控制器或处理器。该芯片内置 320 KB SRAM/128 KB ROM，可通过 SPI/QSPI/OSPI 等接口外接 flash 和 RAM。ESP32-S2 支持多种低功耗工作状态，能够满足各种应用场景的功耗需求。芯片所特有的精细时钟门控功能、动态电压时钟频率调节功能、射频输出功率可调节功能等特性，可以实现通信距离、通信速率和功耗之间的最佳平衡。

　　ESP32-S2系列模组ESP32-S2芯片提供丰富的外设接口，包括 SPI，I2S，UART，I2C，LED PWM，LCD 接口，Camera 接口，ADC，DAC，触摸传感器，温度传感器和多达 43 个 GPIO。它支持芯片外围扩展 PSRAM，ESP-12K 模组可以选配 PSRAM。此外，它还包括一个全速 USB On-The-Go (OTG)接口，可以支持使用 USB 通信。

　　乐鑫官网出售有ESP32-S2 Beta芯片和ESP32-S2芯片，而ESP32-S2 Beta芯片与最终的ESP32-S2不同，Beta芯片是有限的工程样本，因此并非所有功能都可用。我司并没有出售ESP32-S2 Beta芯片的模组！

-  `ESP-12K模组样品购买 戳我 <https://item.taobao.com/item.htm?id=620794721326>`__
-  `ESP-12H模组样品购买 戳我 <https://item.taobao.com/item.htm?id=634951579937>`__


如何搭建开发环境
~~~~~~~~~~~~
    与ESP32的开发环境一致，支持Linux、Windows和Mac系统，
    视频教学：https://www.bilibili.com/video/BV1Ke411s7Go