ESP32-C3系列
====


简介
~~~~~~~~

　　ESP32-C3系列模组ESP32-C3芯片搭载 RISC-V 32 位单核处理器，工作频率高达160 MHz，支持二次开发，无需使用其它微控制器或处理器，是一款高集成度的低功耗 Wi-Fi和蓝牙系统级芯片(SoC)，专为物联网(IoT)、移动设备、可穿戴电子设备、智能家居等各种应用而设计，支持Wi-Fi IEEE 802.11b/g/n 协议和BLE 5.0协议栈，仅支持2.4G频段，不支持5G频段。
    
    ESP32-C3系列模组ESP32-C3芯片提供丰富的外设接口，包括UART，PWM，SPI，I2S，I2C，ADC，可以支持使用 USB 通信。


-  `ESP-C3-12F模组样品购买 戳我 <https://item.taobao.com/item.htm?id=645809822596>`__
-  `ESP-C3-13模组样品购买 戳我 <https://item.taobao.com/item.htm?&id=645141568254>`__
-  `ESP-C3-32S模组样品购买 戳我 <https://item.taobao.com/item.htm?&id=645460125031>`__
-  `ESP-C3-13U模组样品购买 戳我 <https://item.taobao.com/item.htm?&id=645542509201>`__
-  `ESP-C3-01M模组样品购买 戳我 <https://item.taobao.com/item.htm?&id=645896290930>`__
-  `ESP-C3-M1模组样品购买 戳我 <https://item.taobao.com/item.htm?&id=660139048644>`__


如何搭建开发环境
~~~~~~~~~~~~
    与ESP32的开发环境一致，支持Linux、Windows和Mac系统，
    视频教学：https://www.bilibili.com/video/BV1Ke411s7Go

