安信可 LoRa/LoRaWan系列模块 FAQ
======

.. Raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------

Ra-01模块和Ra-02模块的区别是什么？
--------------
答：Ra-01，Ra-02内部只有一个SX1278芯片，两者的区别就是一个天线座子（Ra-02多了一个Ipex座子）

RHF78-052模块用OTAA入网模式失败了，显示join failed 有什么办法去定位失败原因吗?
--------------
AT+CH=CN470 设置为该地区信道

SX1278有寻址或数据加密功能吗?是不是别人的1278只要与我同频就可以收到所有数据?
--------------
	答：没有寻址加密，同频都可以收到。

LoRa模块如何进入低功耗模式（睡眠模式），低功耗模式下功耗是多少？
--------------
通过SPI通信写寄存器的方式进入低功耗模式，低功耗模式下电流为0.2uA（微安μA）------注：此方法不适用Ra-06

LoRa模块上使用的是普通晶体还是TCXO石英晶体，频点为多少？
--------------
答：使用的是普通晶体，频点为32Mhz

关于SX1278射频模式切换的引脚要如何设置？
--------------
答：LoRa模组没有射频开关芯片，不支持这样切换，直接通过SPI配置寄存器就可以使用LoRa模组

不同的LoRa模块之间通信，可以给LoRa模块加地址吗，用来区分是哪一个LoRa模块?
--------------
答：可以，在有效负载的前两个字节定义为地址

信号带宽（BW），符号速率（Rs）和数据速率（DR）间的关系是什么？
--------------
Rs=BW/(2^SF)、DR= SF*( BW/2^SF)*CR，但我们建议你使用Semtech LoRa调制解调器计算器按照不同的配置选型评估数据速率和传输时间。

LoRa设备天线上可以达到的实际Tx功率是多少？
--------------
在芯片引脚输出的功率是+20dBm，经过匹配/滤波损耗后在天线后，在天线上功	率是+19dBm +/-0.5dB。最大输出功率在不同的地区有不同的规定，LoRaWAN规范定义了不同地区不同的输出功率使链路预算最大化。

当两个LoRa模块不能相互通信时，故障检测的步骤是什么？
--------------
	答：首先，在两个设备间检查由晶振引起的频率偏移。带宽（BW）、中心频率和数据速率这些都源自晶振频率。其次，检查在两边的软件/固件设置，确保频率、带宽、扩频因子、编码率和数据包结构是一致的。

LoRa模块可以发送或者接受一个无限长度的有效载荷数据包吗？
--------------
	答：不可以，在LoRa发送或者接受中最大数据包长度是256个字节

LoRa模式的有效载荷长度可以用任意数据配置为256个字节吗？
--------------
	答：SX127x LoRa设备在LoRa模式中有一个256字节的FIFO。理论上，所有的256字		 节都可以用于TX或RX。然而，用低数据速率配置，256字节有效载荷的传输时		间将会很长（几秒或更长），这对于抗衰落和高干扰环境是不好的。在大多数环境		 中这不是一个健壮的配置，因此建议如果想要一个使用低数据速率长的有效载荷，		 那么数据包可以分成几个短的数据包。

LoRa网关的容量是多少？一个网关可以连接多少个节点？
--------------
答：首先最重要的是，容量是在一定时间内节接受数据包数量的一个结果。一个SX1301	有8个通道，使用LoRaWAN协议每天可以接受接近150万包数据。因此，如果你的应用每小时发送一个包，那么一个SX1301网关可以处理大约62500个终端设备。

如果不能达到+20dBm，如何解决输出功率问题？
--------------
   答：1.请确认你连接到正确的引脚（PA_Boost）设置，20dBm输出的引脚。每个频段有两个输出端口。一个是高功率端口称为PA_boost，另一个是高效端口称为RFO。
	2. 然后，检测软件配置。应该正确地配置好三个寄存器：RegPaConfig、RegOcp 和 RegPaDac。这意味着你在软件中应选择正确的引脚做相应的输出，再依据你需要的功耗级别设置正确的值
	3. 确认他们与Semtech参考设计相一致以便设计一个好的PCB布局。这对于可能达到最大的输出功率来说是重要的。

LoRa通信距离和什么有关？
--------------
	1，发射功率。
	2，天线角度，通常天线应该相互平行。
	3，天线的中心频率是否与模块通讯频率匹配。每个天线都有一定的频率范围，称为带宽，在这个范围内，天线阻抗最小，效率最高，这个范围的中间最佳点即中心频率，驻波比最小。
	4，LoRa的配置参数。空中速率越高，LoRa发送数据的速率越快，但是接收灵敏度就降低了。所以要提高通讯距离，就需要适当降低空中速率。空中速率大小和扩频因子SF、带宽BW、编码率有关。空中速率与扩频因子、编码率大小反相关，与带宽正相关。例如，远距离测试时，参数可以这样设置：扩频因子:12；带宽BW:6；编码率:2，注意在低速率通讯时，开启LoRa低速率优化功能。
	5，程序错误。典型的是PA设置错误，LoRa 1278/1276有三个射频信号发送口，分别是RFO_LF: 低频率发送口，效率高；PA_BOOST: 20dBm信号放大口，安信可的Ra01,Ra02使用的是该发送口，程序里务必开启；RFO_HF: 高频率发送口，效率高
	根据硬件原理图连接，选择相应的发送口。

Ra-01这个可以用线连单片机吗？
--------------
	答：可以   通过单片机的SPI协议进行驱动

"RHF76-052可以传输多远
--------------
	答：2公里

Ra-06 里边的 MCU，我们能用来开发传感器吗 
--------------
	"答：不支持二次开发 

LoRa测试板底座的功能是什么
--------------
	"答：MCU驱动LoRa模块，MCU型号是STC15W408AS

模块的使用的晶振是多少MHz的？ 能否提供STM32的demo代码
--------------
	32晶振，我们官网有STM32的代码哈

Ra-02支持点对多吗 ？支持一对多吗
--------------
	支持，只要同频

Ra-01与Ra-02增益大概有多少？ 
--------------
1.5db

Ra-01模组上的晶振是多少频率的？需要这个参数
--------------
使用的是普通晶体，频点为32Mhz

Ra-01和Ra-02距离是多远
--------------
	正常可以达到3km 

Ra-06可以和LoRa网关配合吗 ？RHF76-052可以吗 ？Ra06只能点对点传输
--------------
	不行，点对点通信，或者用一个Ra-06作为网关端开发使用

LoRa的模块有集中器的么 
--------------
	没有 ，这个只是一个射频芯片 

Ra-02模块与MCU通信是什么协议？
--------------
	SPI协议

Ra-01模组能配置成20dB的发射功率吗？我看你们模组上标的是pa+18dB，最大发射功率是18dB?
--------------
	可以配置成20dB，但是实际有线损，实际上是少一点的

Ra-02支持470MHZ吗？ 
--------------
	支持的

Ra-01与Ra-02支持自组网吗
--------------
	可以支持组网，但是需要网关

LoRa测试版可编程吗 
--------------
	测试板可以编程

Ra-06接口是什么呢，SPI还是UART
--------------
	UART，SPI是01 02

LoRa模组与LoRaWAN模组这两个区别是什么 
--------------
	一个是射频模块，一个是集成LoRawan协议的模块 

Ra-02可以星型组网么 
--------------
	可以的，但需要网关 ，我们店铺没有网关，网关是很大一台设备 

Ra-06  能与Ra-01互通吗？
--------------
	可以 ，只要参数设置对

你们LoRa模组最远能到多少距离
-------------- 
	3公里左右，空旷的地方（Ra-06）

LoRa测试版用什么样的烧录器啊
--------------
	CP2102

Ra-02 和Ra-06 待机状态下的功耗为什么标的差这么多 
--------------
	Ra-01和02有衬底电流，06在原本的基础上进行了改良

LoRa模块组网可以多个模块接入一个进行数据传输吗？
--------------
	可以，只要频率相同

Ra-01通信距离能不能加长 
--------------
通信距离能加长，推荐使用棒状天线，需要专门频段的棒状天线哦，还有要注意实际的环境，如果有墙或者楼房阻挡会干扰信号。

你们没有SPI接口的LoRa模块吗 
--------------
	Ra-01  Ra-02都是SPI接口

这款Ra-01模块的DIO0~5是可编程的，对不对 ？有说明资料吗 ？我想让它接IIC传感器可以吗 
--------------
	不行哦，Ra-01 02只是射频芯片模块，模块不能单独当做单片机进行使用 

Ra06可以编程吗 
--------------
目前暂时不开放编程

LoRa模块Ra-02发射功率能持续工作在20dbm下么，还是最高只能设18dbm
--------------
可以持续工作在20dbm

Ra-02支持配置不同信道吗 ？有多少个频率可以设置 ？怎么配置频率
--------------
	Ra-02只能设置频率，不能设置信道。433频率和470频率 。通过软件，自己编写MCUA代码设置。

sx1278理性支持配置信道呀？Ra-02模块的接口协议呢 
--------------
	不支持配置信道，接口协议就是SPI协议


LoRaWAN节点模块，跟LoRa模块Ra-02是否可以互相通信？ 
--------------
	不可以，使用的协议不一样 

LoRaWAN节点模块里面的MCU，可以支持二次开发吗？
--------------
	不支持

Ra-01开发方式模式是用两块单片机分别连接两块Ra01吗?Ra-01本身是有程序了，能改动的吗 
--------------
	用单片机分别连接2块Ra-01 .本身是射频芯片 只能通过单片机驱动

两个射频芯片有“对码”这一说的吗 ?是一个发了另一个就能收到?
--------------
	有的,这个是收发芯片,收发参数必须相同


Ra-01是不是可以同时作为发送和接收的？可以一个与多个连接吗？
--------------
	可以作为发送和接收，但是不是同时。可以一个与多个连接

Ra-01在125MHZ带宽下，正常距离是多少？
--------------
	500米

Ra-02所有433的都通用么？天线那是不是dbi越大信号越强 
--------------
	都可以使用，只要频段在433Mhz的就行。DBI越大信号越强，但是方向性越差 

Ra-02可以用在智能家居产品上吗？ 
--------------
	可以的，具体实现需要您自行研究

Ra-01H模块能到多少公里呀 
--------------
	3-4KM

Ra-06是半双工还是全双工
--------------
	半双工

你们的这个at指令LoRa和LoRaWan是共用的吗? 
--------------
	不共用，Ra-01和02都不是发送AT指令，是通过单片机操作寄存器来进行通讯的

LoRa支持FSK和OOK吗？
--------------
	可以支持,之前发送的示例是LoRa模式  如果想使用FSK模式需要您自己重新编写程序

Ra-01模块uart口波恃率多少，可否修改波特率和地址？如何改？
--------------
	Ra-01是SPI通讯，不是串口哦

Ra-01的频段是可以自己软件配置的吧？433和470之间切换？
--------------
	410-525是可以调试的，最好的频段是433/470

LoRa传输距离多远？实测多少米？待机电流多大的 
--------------
	最远可达10公里，但是传输速率慢，要等很久才会收到信息。在海边空旷环境下，天线距离地面3.5M，实测10KM/1.5mA.

Ra-02可以用PCB板天线么 
--------------
	不可以，要使用PCB板天线用Ra-01

Ra-02在市内能到多少？
--------------
	市内干扰要是比较多的话，对信号强度有影响， 一般能到3km左右

如何设置LoRa前导码寄存器地址呢 
--------------
	通过SPI协议读写寄存器

LoRa收发切换是怎么做的？移植时不知道如何切换
--------------
	通过SPI协议改变这个寄存器的值，这个寄存器是引脚映射的功能，具体是DIO0引脚

LoRaWAN进入低功耗电流是多少？
--------------
	低功耗能达到0.2uA 

贵司LoRaWan模块支持LoRawan协议的Class A和C么？
--------------
	支持的

LoRawan这个支持二次开发么 
--------------
	LoRawan模块不支持开发，LoRa模块需要外接MCU进行驱动

LoRa模块支持透传吗？ 
--------------
	不支持透传

LoRa测试板这里面是什么单片机？
--------------
	STC15W408AS 


LoRa不是局域网的嘛，和云怎么对接 
--------------
	LoRa不是局域网，LoRa是射频通讯，跟云无关

用Ra-01的FSK模式，发现模块没有收发切换引脚，对接收会造成影响吗 
--------------	
	通过DIO映射去控制模块的收发

Ra-01发射功率多大 
--------------
	18+-2，功率是是100mW


Ra-06发射功率是可设的吗？ 
--------------
	可以，通过AT指令设置

Ra-06串口参数出厂默认是多少？
--------------
	9600 偶校验 1位停止位

Ra-02收和发的转换时间是多少？
--------------
	这个是可以自定义的，推荐转换时间为100ms以上

LoRa可以防止数据碰撞吗 
--------------
	可以，但是要您自己编写程序

LoRa一对多的时候，终端可以主动上报吗
--------------
	通过逻辑程序实现

LoRa测试板带空中唤醒吗?不是测试板吗?原本的不带吗 ?原本是空的？
--------------

这个需要您自己去写代码逻辑。原来的程序只是用来通讯使用，没有做空中唤醒的功能

LoRaWAN这个支持多少个组网通信
--------------
	需要根据您自己代码逻辑实现组网哦 

Ra-01没有发射和接收控制引脚，那怎么控制收发呢 
--------------
	通过操作DIO0引脚

Ra-06模块的接收灵敏度到底是多少？ 是在哪个波特率下测试到的？
--------------
	 433频段  SF=7  BW=125KHz 测出来的接收灵敏度为-125 

1278有spi接口的模块吗？功率大于等于1w的
--------------
	Ra-01和Ra-02模块哦

两个Ra-02模块之间可以直接组网通信吧。 
--------------
	可以组网通信，但是需要您自己去代码实现，我们给的驱动代码只是点对点通讯的，没有组网

Ra-01的收发切换是怎么实现的？ 
--------------
	通过更改DIO0的电平

想要1对50的无线组网 ，这个Ra-01模块可以实现吗 
--------------
	需要配合网关

Ra-01H的腳位跟 Ra-01是一樣的吧?? 
--------------
	一样的

Ra-06 其他未使用到的管脚是否可以悬空处理？ 
--------------
	可以哦 

Ra-02是否支持470M频段
--------------
	支持的，410-525

单独LoRa扩展板接电脑是不是可以作为LoRa终端工作？
--------------
	不行哦，必须要有开发板 

LoRa系列的是不是都不支持二次开发  自己烧写 
--------------
	是的

Ra-06  这个模组能不能休眠呢？ 
--------------
	有休眠模式哦


Ra-01模块能支持天猫精灵吗 ？那你们能支持什么平台呢 
--------------
Ra-01不支持天猫精灵。不支持任何平台 ，它只是一个点对点通讯的模块

Ra-01组网的话，有数量限制吗
--------------
	理论上没有哦 

这款Ra-02设备可以同时连多少台LoRa设备？软件最多能实现连接多少台呢？ 
--------------
	这个是要通过软件去实现，两万五台

Ra-02这DIO0\1\2\3\4 这几个管脚干嘛用的？ 要接吗 
--------------
	这几个是控制数据发射和接收的，具体可以查看我们LoRa的芯片手册

Ra-06是需要单独购买网关，还是Ra 06.有网关模式 
--------------
	Ra06有网关模式 

Ra-02模块 通讯距离有多远 ？有障碍物呢 
--------------
	空旷的距离下，距离是10KM。有障碍物会有一定的干扰性，一般会降低5%的距离

LoRa测试版可以修改哪些参数 
--------------
	频率 带宽 发射功率等

Ra-01待机电流是多大啊?能不能做到2uA 
--------------
	1.5mA。不能，这个不是待机功耗，是睡眠功耗，深度睡眠功耗0.9uA

Ra-06模块是用的私有协议？ 
--------------
	是的，私有协议

Ra-02，支持发送与接收的全双工通信吗 ？支持同时进行发送与接收吗
--------------
	全双工，支持同时进行发送和接收


Ra01用弹簧天线能实际测试能发送多远 ？正常楼宇及城市间呢 
--------------
	空旷的距离下，可达到10KM。楼宇城市一般可以到3KM;

Ra-02 SLEEP电流多大？ 
--------------
	5uA以下

Ra-02最好的灵敏度是-141？ 这时候带宽最窄是125k？ 码率配到多少？ 
--------------
	扩频因子设置成12，带宽125K，编码率4/5

Ra-01和sx1276核心有什么区别？代码可通用吗
--------------
	代码可通用

Ra-02的1脚是地吗 
--------------
	是的，Ra-02是IPEX座子

Ra-02用哪种天线
--------------
	Ra-02配的是433M频段的天线，IPEX头的，您发图片的这种是SMA头，如果要使用这种，需要购买IPEX转SMA的天线

Ra-06模块支持中继吗 ?距离能叠加吗?
--------------
	支持的.不能叠加。模块和模块之间的通讯距离是固定的 .是的

Ra-01Ra-01可以烧录么？ 能写程序么？ 
--------------
	不行，不支持二次开发

sx1278芯片可以进行proteus仿真嘛？ 
--------------
	不可以

Ra-06是透传的吗 
--------------
	Ra 06不支持透传

Ra-02低功耗模式下待机电流多少 
--------------
	1.5mA 

Ra-06是一定要接外置天线呢？ 
--------------
	要外置天线

Ra-06拿来没有天线如何调试呢 
--------------
没有天线也可以进行调试，只是传输距离不会太远 .调试方法参考这个网址 https://blog.csdn.net/Boantong_/article/details/104549973


Ra-06买433MHZ的天线就可以了
--------------
	是的

这款LoRa-02模块支持空中速率0.5 kbps吗？ 
--------------
	支持的

LoRa可以中继不？ 
--------------
	可以

Ra-02用哪种天线？
--------------
	433M和470M的天线都可以用的


Ra-01/02/06/01H， 这4款都可以和网关组网吗？ 
--------------
	可以的

Ra-01点对点的通信吗 ?支持LoRaWAN协议么 ？服务器呢 
--------------
点对点通讯是最基本的，如果需要支持LoRawan请在自己的MCU进行实现。服务器这个需要您自己搭建 

Ra-02可以放在设备的壳体里面，还有信号么？
--------------
	壳体不是金属就有 

Ra-01模块休眠电流能达到多少？ 
--------------
	5uA

请问 Ra-02 速率 300kbps的时候距离可以到多远？ 
--------------
	一般能达到3公里 

Ra-01可以图传吗？ 
--------------
	不可以

Ra-02的最低功耗电流是多少 
--------------
	0.5uA 

LoRa集中器私有协议网关， 是否可以连到MQTT？ 
--------------
	可以连接到MQTT 

Ra-02 没有天线引出口么   
--------------
	您要引脚引出的话，可以购买Ra01 

有组好网的LoRa模块吗 
--------------
	Ra-06，不过要配合我们的网关

Ra-01和02的区别
--------------
	01需要焊接弹簧天线  02多了一个IPEX座子  直接找带IPEX座子的胶棒天线上去插上去

LoRa网关能覆盖多少米 ？信号稳定的情况 
--------------
	3km

网关不能直接连接自己的私有云吗 
--------------
	可以直接连接私有云

Ra-02模块我要从板子上拆下来，热风抢温度大概要多少度 
--------------
	400度

Ra01 睡眠状态 电流多少呢 
--------------
	睡眠模式这边测试过5uA

Ra-02能接入阿里云物联网平台吗 
--------------
	不能接入哦

Ra-02的休眠功耗有多少 
--------------
	"3uA

Ra-01的最低功耗就是待机么 
--------------
	待机功耗跟睡眠功耗不一样


Ra-07 和Ra-06可以通讯吗？ 
--------------
	不可以通信，内部的通讯协议不一样，06是私有协议，07是LoRawan协议 


这个Ra-02 ipex是1代还是4代 
--------------
	亲，标准品一般都是使用1代的IPEX座子，这个是为了契合我们的天线

LoRa测试板可以点点互通吗
--------------
	这个只是测试点对点通信

Ra-02控制发送接收 是怎么控制的啊
--------------
	通过SPI通讯协议修改寄存器值，具体修改的方式，您可以查看芯片手册


Ra-02怎么设置为470兆的频段 ？模块的频率选择 
--------------

	通过软件改写相关的寄存器值

LoRa和LoRawan有啥区别呢 
--------------
	https://blog.csdn.net/kangerdong/article/details/92395165

Ra-02两个模块可以用同一个天线嘛  
--------------
	可以

Ra-01数据发出到收到回复的时间 延时多少 
--------------
	1S

LoRa数据传输最快可以达到多少呢 ？速率 10米距离左右 可以达到吗
--------------
	每包数据是256字节每秒 ，速率不高的哦。每包数据是256字节每秒 ，速率不高的哦

LoRa测试板可以发指令更改频率吗 
--------------
	不行哈，要修改频率需要通过修改源代码

Ra-06与Ra-01的信号可以互通吗？比如用Ra-01做为子设备，Ra-06做为网关设备
--------------
	只要参数设置没问题就可以互通

Ra-01可以和手机透传吗 
--------------
	不支持透传 

Ra-01 带中继功能么  
--------------
	中继功能需要您自己在单片机实现

学校宿舍超表，300个点，需要发送到中点。有推荐的模块方案吗？ 
--------------
	推荐LoRa

Ra-01模块能跳频吗
--------------
	可以跳频

LoRa转接板是直接连上单片机就能用吗？ 
--------------
	可以，通过单片机的SPI协议进行驱动

Ra-02型号的模块，可以低功耗运行吗？ 
--------------
	可以的
Ra-01H模块默认是868mhz的吗 ？还是要切换的 
--------------
	软件定义的哈，通过单片机进行设置的

Ra-06不用IPEX座，怎么弄
--------------
	通过更改电阻

Ra06一次性能发送多少个字节数据？？ 
--------------
	250个

Ra-02里面下载固件了吗 
--------------
	Ra-02只是一个射频模组,它需要外接MCU通过SPI协议进行寄存器的操作

Ra01模块的RESET管脚可以悬空吗 
--------------
	不可以悬空，是必须接的

Ra-01这个待机状态下，能外部无线电唤醒耗电最小电流能到多少?可以到uA级么
--------------
	1.5mA左右，uA级是睡眠状态


Ra-07需要外挂mcu？不是带有mcu了吗？
--------------
	自带MCU是LoRa芯片，实际使用还需要外挂MCU

Ra-02发送的话输出是多少w？
--------------
	20db
	
LoRa有arduino例程吗？
--------------
	这边不提供arduino例程哦，您可以自己在网上找找哦

Ra-01能不能组网？是不是链接串口就能发送
--------------
	不行，Ra-01模块是射频模块，需要MCU驱动

"Ra-06这一款是邮票孔还是通孔呢？
--------------
	邮票

Ra02是半双工通信吗
--------------
	是的

Ra-06还需要自己开发吗？还是拿到后配置一下就能使用
--------------
	不需要开发，拿到后配置就能使用。https://blog.csdn.net/Boantong_/article/details/104549973  具体使用您可以查看这个资料

Ra-06能做透传？
--------------
	不做

Ra-02是不是做不了网关的
--------------
	可以做网关，但需要您自己写相关协议

Ra-06能和其他的1278配套使用吗
--------------
	可以配套使用，但是相关参数需要自行适配

Ra-07或Ra-07H可以直接工作在串口模式下吗
--------------
	可以直接工作在串口模式下

Ra-01用这个天线传输距离可以多远？与外置天线效果一样吗？
--------------
最远可达10公里，但是传输速率慢，要等比较久才会收到信息。在海边等空旷环境下，天线距离地面3.5M，实测10KM

Ra-01的模块，请问您知道怎么读取它的寄存器嘛
--------------
	通过SPI协议

Ra-07H可以用来做对讲机吗，语音包有带压缩吗
--------------
	可以做对讲机，语音包压缩需自行实现，LoRa只是用来传数据

Ra-02为啥 发射只到18 dbm？
--------------
	18+-2，最大可以到20的

Ra-01转接板的尺寸有吗、
--------------
	26mmx26mm

Ra-01S支持组网吗
--------------
	支持

Ra-01S在海面上能传输多远
--------------
	11公里

Ra02这款模块可以连接网关吗
--------------
	可以连接， 但是需要您自行在单片机上实现网关协议

购买Ra-01转接板的话，直接用杜邦线连接到单片机，可以用吗？
--------------
	可以，但模组必须接单片机;


Ra-02实际上你们的天线和GND是连在一块的？
--------------
	一端是跟内部射频引脚，一端是跟GND

LoRawan和LoRa有什么区别
--------------
	LoRawan是在LoRa上集成的一个通讯协议

LoRa模块，如何实现的组网？比如我买了3个模块，他们互相之间怎么连接？不能双向通信？
--------------
	两个发送 一个接收。模块是收发模块 ，可以当发送可以当接收，需要通过软件设置

Ra-01S供电是要3.3V吗？功率1W就够了吗？
--------------
	可以的

Ra-07支持阿里云MQTT吗
--------------
	可以对接阿里云linkwanhttps://aithinker.blog.csdn.net/article/details/107989571

有230Mhz的LoRa模块吗
--------------
	Ra-01H

Ra-07模块可以直接用串口软件测试吗？通过串口发送命令测试
--------------
	可以的

Ra-01是不是有协议？？
--------------
	只是一个射频模组，有SPI协议

Ra-01, Ra-01s封装一样吗
--------------
	封装一样的，但是引脚定义不一样

Ra-07可以跟Ra-02通信吗
--------------
	不行哦

Ra-01s有没有转接板，跟Ra-01是不是兼容
--------------
	跟Ra01兼容

Ra-01S是透传模块么？请问支持中继么？
--------------

	是的。中继需要您自己再单片机上进行实现，模块只是一个射频模块

Ra-01 模块用不了  RG-01 ？
--------------
	目前Ra-06和RG01进行通讯，Ra01的还在进行适配

Ra-01/02需要两种模块才能完成发送和接收是吧
--------------
	模块是收发模块，但是测试需要两个，一个设置成发送，一个设置成接收

Ra-01最远通信多少米？
--------------
最远可达10公里，但是传输速率慢，要等比较久才会收到信息。在海边等空旷环境下，天线距离地面3.5M，实测10KM

Ra-01是标准的LoRa协议吗
--------------
	是的，需要使用单片机驱动模组，模块只是一个射频器件

这个Ra-01S能传多远在树林
--------------
	5公里

Ra-01模块可以读取信号强度吗
--------------
	可以的，您可以查看寄存器手册

Ra-01s的模组是否能和你们的网关通信？
--------------
	可以和网关通讯，需您自行实现相关通讯协议

Ra-01S TXEN和RXEN是什么功能？怎么用的？
--------------
	这个是接到射频电路上的，具体您可以查看规格书中的模块内部原理图

Ra-02的距离多少
--------------
	市内干扰要是比较多的话，对信号强度有影响， 一般能到3km左右

Ra-01S发射功率可调吗？ 
--------------

	可通过操控寄存器进行设置

Ra-01H 有它配套的天線嗎
--------------
	频段在868 、915频段都可以

Ra-01S模块和Ra-01模块，在程序的编写上有区别吗？引脚定义是否一样
--------------
	一样的，区别具体您可以查看我们的示例

Ra-01S跟之前的Ra-01在封装和程序上是不是都兼容的
--------------
	兼容

Ra-01S可以多个发射，一个接收吧
--------------
	可以的

1268与1278在LoRa模式下能否通信？
--------------
	可以的

LoRa-01模块有中继功能吗？
--------------
	需MCU实现

Ra-01没有找到具体功能模式的资料呢
--------------
	您可以查看sx1278中文数据手册，https://docs.ai-thinker.com/%E5%8E%9F%E5%8E%82%E8%B5%84%E6%96%99

Ra-06支持空中唤醒吗
--------------
	它有CAD模式，具体您可以看看指令集

Ra-01不加弹簧天线的通信距离大概为多少呢
--------------
	接了天线的情况，空旷 2公里没什么问题的。不接天线就几厘米

Ra-06  CAD模式的话  功耗有多大  唤醒需要多久？
--------------
	7uA，唤醒需要1S

Ra-06模块的硬件资料
--------------
https://docs.ai-thinker.com/_media/LoRa/hardware/Ra-06_v1.3.zip

Ra-07和Ra-08有什么主要的区别
--------------
Ra-08除了支持jink烧写外还支持串口烧写，方便做二次开发使用

LoRa模块可以组网不
--------------
可以组网，但是组网功能相关代码逻辑需要自行实现

LoRa模块影响通信速率和距离的两大重点原因是哪些。
--------------
扩频因子和带宽，扩频因子越大距离越远，但是速率比较慢；带宽越宽速率越快，但是距离较近，两者相反。

LoRa模块一次最大可以传输多少个字节
--------------
250个

Ra-08模块能否点对点通信
--------------
出厂固件不支持，可以通过二次开发的方式进行实现

Ra-01的测试板能给Ra-01S使用吗
--------------
不能，因为驱动不同

Ra-08烧录串口和通讯串口的发送引脚是否共用
--------------
共用

Ra-01以及Ra-01S模组的引脚驱动有区别吗？
--------------
有的，Ra-01模组中断提示引脚为DIO0，而Ra-01S则用DIO1以及busy结合；

Ra08模块出厂固件支持点对点传输吗
--------------
不支持，出厂固件仅支持与LoRawan网关进行通信，如需点对点传输得需要二次开发实现

Ra-01S模块在使用的时候是否需要接上TXEN RXEN引脚
--------------
不需要，我们通过软件控制射频收发，不需要硬件控制

Ra-01、02模块支持自定义LoRawan通讯协议吗
-----------------
支持，可以在MCU上实现LoRawan相关的功能

Ra-07、08模组能否当LoRawan网关使用
------------------------------
不能，单模组仅支持LoRawan节点

Ra-01S和SC上的TXEN、RXEN引脚是做什么用处
--------------------------------------
这个是射频天线开关引脚，控制射频收发使用

Ra-08H支持AS923频段吗
-------------------
支持，需要烧写支持AS923频段的固件

Ra-07/08LoraWan模组有几个接收通道
----------------------------
 8个。

Ra-01S和SC跑官方驱动需要接TXEN,RXEN引脚吗
----------------------------
不需要，接线除了常规的SPI引脚外，只需要接RESET,DIO1,BUSY引脚

Ra-08模组能当网关使用吗
----------------------------
不能，模组只能当节点使用，不能当网关

LoRa的通讯方式是全双工还是半双工
----------------------------
半双工的方式

LoRa模块不配天线有什么影响
----------------------------
模块没有天线会出现距离短 经常丢包的情况

有带中继功能的LoRa模组么？
--------------------------
LoRa模组可实现中继功能，但需要客户自行设计该功能

Ra-08的二次开发的demo编译好的固件烧录地址设置为什么？
--------------------------------------------------
烧录地址为0x08000000

Ra-08H模组为何无法入网TTN，用的网关是US915网段？
-------------------------------------
查清楚，a、模组是否已经烧录了US915网段的固件；b、查下模组的掩码配置是否与网关一致

Ra-08模组在上电可看到log信息，但是发送AT指令没有反应是什么原因呢？
------------------------------------------------------------
RX口没有选择LPRXD接口

Ra-01模组与Ra-02模组的转接板是否兼容的呢
------------------------------------
转接板兼容Ra-01、Ra-02、Ra-01H、Ra-01S、Ra-01SH以及Ra-01SC模组，单驱动引脚的解析不完全一样

Ra-01SH默认不带IPEX座子，补上IPEX座子能插IPEX线使用吗？
-----------------------------------------
补上IPEX座子能插IPEX线

Ra-08模块在设计上需要注意哪些引脚
--------------------------
IO2引脚和IO11引脚，IO2为烧录引脚，正常使用需要保持低电平或者悬空，IO11引脚为系统引脚，正常使用必须为高电平或者悬空

Ra-01S中的DIO1不使用中断触发的形式，可以修改代码实现收发模式切换吗
-------------------------------------------------------------
可以通过修改代码实现，但是需对寄存器手册比较熟悉

LoRa测试底板能焊接哪些模组，使用上有什么需要注意的
---------------------------
测试底板能焊接Ra-01/02/01S/01SC等模组，其中，Ra-01/02在STM32例程上需要接DIO0引脚，Ra-01S/SC需要接DIO1和BUSY引脚

Ra-08开发板的Class B入网方式，重启的情况下数据无法传输到服务器，显示计数不对，是为何？
-----------------------------
Class B入网方式不需要做入网动作，因此重启的情况下，计数将重置为0，而服务器的计数值不变，因此，一旦计数值对不上，服务器将丢弃该数据。

Ra-02模组的ipex接口是什么规格的？
-----------------------------
我司所有带IPEX底座的模组均是搭配的IPEX 1代底座。

Ra-02模组我不小心用螺丝刀碰一下就没信号了，但是如果MCU重启又有信号了，是什么原因呢？
-----------------------------------------------------------
天线短路了，需分离之后重置射频发射或者接收功能。

Ra-08中的SPI引脚是否就是规格书中标注的SSP引脚？
-----------------------------------
是的。

Ra-01模块内置的SX1278芯片，在初始化配置时PA输出引脚应当设置为RFO还是PA_BOOST
-----------------------
设置PA_BOOST

Ra-01S模块内部芯片外部有没有加PA放大器
---------------------
没有添加放大器

LoRa模块能否用在水表上
------------------
可以

有没有不限包长度的lora模组
----------------------
目前lora模块最大包长度为250个字节，没有不限包长度的

请问Ra-08模组支持设置扩频因子么？
--------------------
支持通过AT指令设置扩频因子，在设置之前实现关闭ADR功能；

Ra-08模块能否当网关使用
--------------------
不能，只能当节点

RG-02支持连接helium平台吗
---------------------------
不支持

LoRa模块的信号能穿透铁皮盒子吗
---------------------------
不能穿透，信号会被铁皮盒子吸收

Ra-01SC模块如果不使用IPEX座子，可以直接焊接天线引脚引天线出来吗
--------------------------------
可以焊接，因为座子和天线引脚是导通的

有对讲机的模块吗
----------------
可以推荐使用2.4G或者lora模块

Lora模块同频情况下如何防止相互干扰
---------------
同频会产生干扰，需要自己做协议筛选信息

Ra-08模组与RG-02网关在470M频段通信需要注意什么
--------------------------------
CRXP指令参数设置505300000，频点掩码需要设置0400

Ra-08模组如果不需要dataDown数据上传该如何做
---------------------
AT+DTRX指令上需要将confirm禁掉

请问一下我用STM32驱动Ra-01模组的时候，debug的情况下可以正常启动模组读取数据，可是直接运行起来又出问题，是什么原因呢？
-------------------
debug模式下，为单步运行，时序可控，因此要检查程序中指令与指令之间是否有时隙。排查指令发送之间冲突原因。

请问一下，RG-02网关可以通过4G与私有服务器通过MQTT通讯么？
----------------------
可以的，请参考说明文档。
https://blog.csdn.net/Boantong_/article/details/126717812?spm=1001.2014.3001.5501

请问一下Ra-01S模组的晶振有温度补偿功能么？
--------------------
该模组用的普通晶振，没有温度补偿功能；

请问一下1276芯片不是全频段的么，为何Ra-01H模组频段为800多到900多兆？
-------------------------------------
模组中的射频电路专门做了射频区间的，该芯片也可以设置其他频率，但是信号会很差；

请问Ra-01模组分主从的吗？
----------------------
该模组为纯射频模块，仅实现数据透传，需MCU驱动，主从功能是应用层面的，需用户开发实现；我们提供驱动demo，请查看链接：https://docs.ai-thinker.com/%E5%BC%80%E5%8F%91%E8%B5%84%E6%96%99

请问Ra-08H模组设置工作频点为923MHz可以么？
------------------------
可以的

请问Ra-08-Kit有EU433固件么？
------------------------
当前没有该固件

RA-01模块能用在打窝船主板上吗
--------------------
可以使用

Lora模块在CAD模式中，SF及BW的参数正常设置多少比较合适
---------------------
根据测试环境进行调整，以实测为准

Lora接收模式空闲状态和接收超时的区别是什么？
--------------------------
空闲状态指的是模块没有接收到发送方的数据，接收超时指的是模块在指定时间内没有接收到发送方的数据

Ra-01SC可以用在arduino和ESP32上吗
----------------------
模组可以使用，有对应的arduino示例

Ra-08支持片上开发吗
-----------------
模组支持开发，有对应的SDK和教程

Lora测试板可以连接电脑吗
------------------
可以，不过需要配合串口板去连接

Ra-08可以用pathy吗
------------------
不可以使用

有没有支持测距的lora模块
-------------------
目前没有，如需测距可以选择UWB模块

Lora模块可以支持MQTT吗
-----------------
不可以，如需使用MQTT必须要接一个WIFI模块实现

请问433M天线跟470M天线用起来一样的么？对信号是否有影响，目前用的470M频段，买433M的天线
-------------------------
可以用的，信号影响不大

请问是否有发射功率大于22db的lora模块？
----------------------
没有

请问RG-02网关最多支持连接多少个设备？多了会有什么后果？
----------------------
200个，多了会丢包严重


请问LoRa模块可以用51单片机驱动么？
---------------
Ra-01模组有51单片机的驱动程序；获取链接：https://docs.ai-thinker.com/%E5%BC%80%E5%8F%91%E8%B5%84%E6%96%99

.. image:: demo.png

RA08开发板使用需要配置外部晶振电路吗
------------
不需要配置，直接插上USB口即可使用

RA01S可以和RA02通信吗
------------
可以 只要lora参数相同即可相互通信

lora的模块支持串口透传使用有哪些模组推荐
-----------------------
可以使用RA-08模组，支持串口透传和lorawan通讯协议

RA-01SC模组适配的收发器模块是哪一款
------------------
RA01,02,01S模组都支持适配

lora模块通过什么方式与ESP模块进行连接
-------------------
通过SPI的方式

RG-02网关通过什么方式连接到云平台
------------
通过MQTT的方式

RA-08开发板可以用windows上的keil软件进行开发吗
-------------------
可以使用keil软件进行开发

请问Ra-01是否满足工信部的微功率短距离无线电发射设备目录和技术要求相关规定？
----------------------
不满足，功率太大。

请问Ra-01SC是否有设置地址的寄存器？
-----------------------
没有。

请问频段923-2，支持Class B的节点与网关选哪？
--------------------
支持该功能的节点选择Ra-08H，需联系安信可工作人员获取固件，我司没有支持Class B的网关。

请问：RG-02网关是否支持sim 4G卡？是否插上去就可以直接用了？
-------------------
支持，需要配置，详情见介绍的博文：
`参考链接  <https://aithinker.blog.csdn.net/article/details/126717812>`__

请问：lora模组是半双工还是全双工的？整机功率多大？
----------------------------
是半双工的，sx127x系列模组最大功率为20db，sx126x是22 db。

如果需要实现8层楼之间的通讯，推荐使用哪款模组
--------------------
推荐使用Lora系列

Ra-01SC封装做了引线到天线，现在使用IPEX版本，原来的封装会因为天线引出影响IPEX的效果吗
---------------------
会影响，建议去掉引线

RA08烧录串口和通讯串口的发送引脚是否共用
-----------------
共用

Ra-01模组天线馈线可否直接焊接在天线接口端子上
--------------------------
可以直接焊接

请问Ra-01模组能接入ESP32么？
--------------------
不可以；

有没有功率放大的lora模组
----------------------
没有，lora模组最大发射功率为22db，通过软件进行设置

Ra-08模组可以做点对点，点对多点的功能吗
-------------
可以，需要更新专门的透传固件

Ra-01SC模组是否支持空中唤醒
--------------------
模组支持空中唤醒功能，具体实现方式可以参考CAD模式相关介绍

Ra-01SC上有3个GND引脚，使用的时候全都要接还是只接一个就行
----------------
使用的时候只需要接一个GND即可

Ra-01SC是收发一体吗
----------------
是的，模组既可以当发送也可以当接收

Ra-08模组二次开发有windows的编译环境吗
------------
可以通过SDK的keil脚本生成keil工程进行编译

Ra-08模块可以跑loramac-node吗
----------------------
可以，通过GCC工具链进行编译烧写使用

Ra-08H默认出厂固件使用的是哪一个频段，是否支持AS923
-------------
默认出厂使用的频段为EU868,如果需要使用AS923频段，可以联系我们获取对应的固件进行更新

Ra-08和Ra-08H可以通信吗
-----------
不可以。这两款模组使用的频段不一样，无法进行通信

请问Ra-08模组能实现收发透传么？
------------------
可以，烧录透传固件即可；

请问Ra-01是否有RXEN以及TXEN两个引脚？若是要切换发送接收要如何切换
----------------------
这两个引脚内部已经做好了控制电路，未引出；收发模式切换通过配置寄存器的方式实现即可；

请问Ra-08的IO口数量较少，请问后期有扩展IO口数量的计划么？
--------------
目前没有；

请问Ra-08模组是否支持I2C实现AT交互功能？
----------------
该功能可实现，但是要用户自行实现；

请问Ra-08/08H是否同时支持ClassA与ClassB？
----------------
支持；

请问Ra-08模组为何LP串口只有接收没有发送呢？
----------------
有的接收端口是UART0_RX；

请问Ra-01可以通过MCU驱动实现lorawan通讯么？
------------------
支持，MCU中做好lorawan协议栈即可实现；

RA-01模组可以读取rssi的值吗
------------------------
可以读取，通过SPI读写RSSI对应的寄存器值

RA-08透传固件发一条数据的时间比较长，可以改善吗
--------------------
不可以，这个时间受lora调制方式限制的

如何增加lora的传输距离
--------------
调整Lora相关的参数值和选择增益好的lora天线

RA-08H支持US915频段吗
----------
支持，不过需要烧写对应的US915频段的lorawan固件

RG-02网关工作温度是多少
----------------
 -20-70摄氏度

Ra-01SC天线孔可以焊接在电路板上面吗
---------------
可以焊接，不过在天线设计的时候需要做好50欧姆的阻抗

Lora可以定点发送数据吗
-------------
不可以，模组不支持该功能

请问Ra-08入网LoRaWAN平台是否支持数据透传？
--------------------
不支持，仅支持指定数据长度传输数据。透传功能需自行开发。

请问Ra-08模组支持监听串口数据，收到数据直接发送到LoRaWAN平台么？
-----------------
该功能需要用户自行开发设计实现。

问请问Ra-02模组410M~535M不同相邻信道间隔至少多大不会互相干扰？
-------------------
至少配置的带宽的两倍。

请问LoRa模组是否可以实现对讲机功能？
-----------------
不可以，数据量太大，实时性太高。

请问为何Ra-08模组烧录了CN470之后，三元组均是00？
-------------------
入网三元组需要到服务器上面申请，并通过AT指令的方式写入模组内部。

请问Ra-02模组支持lora mesh组网么？
-----------------------
支持，mesh组网属于应用层部分内容，要用户在MCU开发实现

请问为何Ra-01SC驱动的时候，写数据到寄存器之后，读出来的数据始终是0xFF？
-----------------
SPI驱动有问题，需要排查一下；强烈推荐用硬件SPI驱动lora模组。

请问Ra-01CS模组的STM32F103C8T6工程是否编译下载好固件即可实现串口收发测试呢？
-----------------------
需要用户手动配置发送或接收模式。

请问Ra-08模组是否只能用lorawan相关功能呢？
------------------
Ra-08支持二次开发，用户可根据自身需求做开发，收发等驱动都是做好了的。二次开发源码： `获取链接  <https://github.com/Ai-Thinker-Open/Ai-Thinker-LoRaWAN-Ra-08.git>`__