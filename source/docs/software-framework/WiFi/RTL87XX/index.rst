安信可 RTL87XX系列模块 FAQ
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


BW16可不可以支持DNS解析？ 
--------------
不支持DNS解析

BW16用蓝牙来搜索配置网络，使智能设备接入WIFI路由器网络，可以实现不 
--------------
这个是可以实现的，具体可以查看我们的指令手册， https://docs.ai-thinker.com/rtl87xx 

BW 16可以wifi 蓝牙同时开启使用么 
--------------
可以使用，具体查看我们的蓝牙配网的功能

BW16 wifi最大传输速度是多少 ？和mcu是啥接口 ？有Spi传输方式吗 ？一般mcu串口都没那么快 
--------------

串口传输，最大传输速度4M。没有SPI ，WIFI传输的方式是通过串口

BW18 蓝牙与wifi传输速率多少能有 
--------------
WIFI速率为938KB/S  蓝牙为2KB/S

WIFI双频2X2 11AC+BT5.0模组 有吗 
--------------
BW16

这个BW16 WIFI 接口是串口吗 ？ WIFI与WIFI之间传输速度有多少？最大 
--------------
是串口，传输速率1Mbit

这个BW16 WI-FI支持2.4G和5G是吗 ?同时支持吗 还是需要配置 
--------------
同时支持

BW16模块，如果CHIP_EN引脚拉高，也就是模块停用情况下，模块电流有多大？
--------------
拉高这个引脚模块就是正常工作了

BW16模块 是透传，还是要有驱动的 
--------------
透传

BW16 AT指令可以提供spi接口吗？mcu上只有spi口
--------------
不提供SPI接口  我们都是串口通讯 ;

目前这款BW16已经接入了哪些平台？ 
--------------
天猫 阿里飞燕

BW16 WiFi频段是自动切换还是手动切换 
--------------
自动切换的


BW16模块支持自动漫游连接吗？ 也就是无缝漫游，即使用区域有多个相同SSID和密码的AP，买的wifi模块在AP之间移动时，能否自动切换AP？
--------------
不支持

BW16模块可以二次开发吗 
--------------
可以二次开发，但是SDK不对外开放哦

请问BW16 LOG_TX与LOG_RX是什么引脚？连接到CPU的什么引脚上？ 
--------------
这个是打印log信息的串口，一般不需要连接CPU，连接CPU只需要接AT串口就可以

BW16支持微信配网吗？
--------------

不支持airkiss，这个配网仅支持simpleConfig。

BW16支持ap配网吗 
--------------
出厂默认的AT固件仅支持SimpleConfig配网和蓝牙配网

"BW16板子可同时支持wifi和BLT吗 
--------------
可以

bw16可以使用spi通讯么？最高通讯速率多少 
--------------
可以使用SPI通讯，但是需进行二次开发，出厂的固件不支持SPI通讯

BW18资料
--------------
https://blog.csdn.net/Boantong_/article/details/104606575


这个BW16连接速度是多少？ 
--------------
150kb/s

BW16支持ADC，是指可以采集模拟量吗？ 
--------------

是的，采集模拟量

BW16支持蓝牙mesh吗？
-------------- 

不支持哦 

BW16两块板子可以通过WIFI互传吗？ 
--------------
可以互传

有同时支持双频段的wifi模块吗？ 
--------------
BW16

BW16可以直接同时使用WIFI和蓝牙吗 ？它不是只有一个天线，如何能同时使用 
--------------

可以的，BW16的蓝牙可以进行配网，蓝牙配网就是一个示例

BW18是可以用来连接阿里云mqtt服务器是吗？ 内部集成了标准mqtt是吗？ 
--------------
是的

BW16支持蓝牙 sig mesh吗 
--------------
不支持

BW18 AT指令设置WIFI mqtt那个怎么操作？
--------------
https://blog.csdn.net/Boantong_/article/details/104791452

BW16用的什么芯片
--------------
瑞昱的RTL8720DN

RTL8710BX RTL8710AF有什么区别？
--------------
只是封装不一样，软件上是通用的

BW12支持二次开发？
--------------
支持

BW12烧入固件，需要什么硬件？
--------------
串口板就可以烧录,使用的uart协议烧录

BW16是双频WIFI是吧，通过串口可以建立几个tcp连接？
--------------
可以建立5个tcp连接

BW16，要外接天线的，下单时备注就行，还是怎么呢？
--------------
外接需要拆开屏蔽盖修改某个电阻，建议您直接定制哦

BW16的蓝牙，除了配网用，我可以自己单独控制使用吗
--------------
二次开发可以单独使用，AT指令只能使用蓝牙配网的功能

BW16多连接，一个客户端，一个服务器，一个UDP广播可以吗？
--------------
可以的

BW16模块配网 有什么资料
--------------
https://blog.csdn.net/Boantong_/article/details/108847305

BW16支持串口透传吗？
--------------
可以的

BW16弄到电路板上连接单片机，用手机app控制灯。如何配置
--------------
您通过单片机去配置，https://blog.csdn.net/Boantong_/article/details/108847305

BW16 wifi和蓝牙都是用at指令来交互吗？
--------------
是用at指令来交互

BW16的wifi和蓝牙共用一个天线是吧？
--------------
是的

BW16蓝牙是不是不能实现透传？只是对wifi的配置用的？
--------------
出厂默认固件蓝牙只能用作配网

BW16开发板支持arduino吗
--------------
不支持

BW16外接定制是不是拆掉个电阻就行
--------------
不是哦，里面电路设计也要改的

BW16模块，通讯距离能达到多远
--------------

wifi40-50米，蓝牙一样的，传输距离受限于发射功率和天线的接收灵敏度


BW16  wifi 和蓝牙可以同时使用吗
--------------
可以的


BW16模块支持MQTT协议吗
--------------
需要二次开发

BW16模块支持域名连接吗
--------------
支持

BW16速率有多少？蓝牙速率
--------------
1Mbit，蓝牙最大30KB/S

BW16那个IPEX座的焊盘也是没用的？具体怎么改？
--------------
预留的，里面需要改电阻。电阻换到R9

BW16可以对mcu编程吗？
--------------
可以，需要C语言基础比较扎实

BW16可以改Mac吗
--------------
可以的

BW16  WIFI和蓝牙，二选一？WIFI双频是同时存在的，还是二选一
--------------
是的，二选一。同时存在

BW16模块可以自动重连吗
--------------
可以自动连接

BW16模块能接到什么云平台呢?
--------------
阿里云

BW16支持什么协议?你们有自已的云吗?
--------------
TCP UDP HTTP MQTT（需二次开发）。目前有一个测试的透传云，仅供测试使用http://tt.ai-thinker.com:8000/ttcloud

BW16 wifi和蓝牙可以连接同时使用？
--------------
可以连接同时使用

BW16开发板带USB转串口吗？是cp2102？
--------------
CH340

BW16描述是接口有SPI，这个SPI是可以传输数据吗？
--------------
需要做二次开发的，AT固件不支持SPI。仅支持串口asd

BW16的WiFi可以在2.4G频段和5G频段同时工作吗？
--------------
可以，因为模组设计的天线有2.4G频段和5G频段，在使用上不冲突

BW16的2.4G与5G频段如何做切换？
--------------
2.4G与5G切换内部已做好的，不能手动切换。若需专门连接5G频段，可连接5G频段WiFi。

BW16作为AP可以连接多少个设备
--------------
3个

BW16蓝牙功能待机电流和发射电流是多少
--------------
蓝牙待机电流为20mA，发射电流为35mA

BW16能否连接2.4G/5G混频的路由器吗
--------------
可以连接。

请问BW16可以开启5G的热点么？
--------------
不可以，BW16的2.4G频段与5G频段是内部做好切换的，不可手动配置

BW16模组支持USB通讯么？
---------------------
不可以，BW16模组没有USB硬件接口

只能看到BW16的蓝牙配网功能资料，BW15的没有，是否不支持
-----------------------------------------------
BW15没有蓝牙配网功能，智能配网功能与BW16是通用的

BW15的蓝牙不是可以给wifi辅助配网么？
--------------------------------
没有开发该功能

请问BW16模组的封装文件是否不支持AD打开
------------------------------------
封装文件包中的.asc文件支持通过AD软件打开

BW15的配网APP有IOS版本的么
-----------------------
有的，与BW16配网的APP是通用的

为何BW16开发板接入电脑不能启动呢
-----------------------------
检查USB口是否保护了；在不断插拔的情况下，有时直接断开电，USB口就保护了

BW16的蓝牙配网功能，支持修改广播功能么
---------------------------------
不支持

BW16模组5G频段能达到的最高速度是多少
---------------------------------
2M以内

BW16 Combo-AT固件和at固件有什么区别
----------------------------
Combo-AT固件支持MQTT功能和蓝牙通信功能

BW16和BW18有什么区别
------------------
芯片不一样。BW16使用的芯片是RTL系列，BW18使用的芯片是ESP32系列

BW16模组连接MQTT有密码长度限制吗
---------------------------
有，MQTT密码长度不能超过64个字节

BW16 arduino开发如果需要指定串口输出可以参考哪一个示例
------------------------------------
可以参考AmebaSoftwareSerial中的SoftwareSerial_Basic示例

BW16模组如何指定工作在5G频段还是2.4G频段？
-----------------------------------------
BW16的工作频段的切换是内部做好的，若是指定5G频段工作的话，可通过连接5G WIFI的方式。

为何给BW16发送指令，老是回复“unknow command”提示呢？
-------------------------------------
通常出现这种提示，表示指令不支持，即：指令有误。

BW16进入透传以后，断开网络，要如何退出透传呢？
----------------------------------------
无论是否断网，均通过发送“+++”退出透传，另外，发送“+++”的时候不得加回车换行符。

BW16WIFI名称和密码最大能设置多少个字节
-----------------------
名称32个，密码64个

请问，BW16开发板arduino开发环境要如何搭建？
------------------------
参考说明链接：https://blog.csdn.net/weixin_51452951/article/details/122286441

请问，BW16模组是否可以直接替换ESP-12F模组？
--------------------------
BW16以及ESP-12F模组的供电以及AT串口是pin to pin的；

请问BW16是否有自动重连功能？
------------------------
Combo-AT固件可以通过配置指令：AT+WAUTOCONN=1，实现重启功能；

请问BW16如何查询wifi连接状态呢？
---------------------
Combo-AT固件通过发送指令：AT+WJAP?，查询wifi连接状态

请问一下BW16集成的MAC是否有ipv6协议栈？
---------------------
有的。

BW16模组可以将配网成功后的WIFI密码读取出来吗
-----------------------
不可以，目前仅支持将WIFI的SSID读取

请问BW16的AT固件与Combo-AT固件之间有什么区别？
------------------------
AT固件是老版本固件，不包含蓝牙功能，Combo-AT固件是安信可设计开发的，功能比较齐全；AT固件与Combo-AT固件的指令完全不一样；Combo-AT固件功能更全，推荐使用；

请问旧版本的BW16开发板为何烧录失败？
-------------------------
MicroUSB接口连接的是模组AT串口，烧录的话，需要借助USB转TTL模块连接Log_RXD、Log_TXD完成固件升级；

BW16同时用WIFI和蓝牙会丢包吗
-------------------
同时用会丢包

BW16的驱动程序和ESP-01S的一样吗
-----------------------
不一样，指令格式不同

请问BW16模组的主频是多少？
--------------------
80M

请问BW16模组可以通过SPI进行通讯么？
----------------------------
可通过二次开发实现；二次开发资料需前报名协议才能释放；


请问BW16模组能连接WPA2企业加密么？
-----------------------
可以通过二次开发实现


请问BW16模组可以通过232串口通讯么
---------------------
可以


请问BW16模组支持PWM输出么
--------------------------
支持，需二次开发

BW16 Combo-AT指令的开发板和旧版AT指令的开发板不通用吗
-----------------
指令格式不同，不通用

BW16的芯片主频是多少
-----------
主频200MHz

BW16支持MQTT接入阿里云物联网平台吗
----------------------
支持的

BW16的蓝牙扫描频率可以通过指令调整吗
-------------------------
不可以，目前没有对应的指令进行设置

请问BW16模组支持STM32驱动么？
---------------------------
支持，直接通过串口发送AT指令的方式实现。

请问BW16的蓝牙扫描频率是否可调？
------------------
没有该功能的指令。

请问BW16没有wifi/蓝牙连接指示灯么？
----------------------
没有。

BW16是否支持HTTP客户端分包获取web服务器文件包？
--------------------
可通过增加Range字段的方式实现分包获取资源功能。

BW16 WIFI模块可以修改WIFI信道吗
----------------------------
可以，通过串口发送AT指令设置模块为AP模式进行修改

BW16配网工具支持IOS手机吗
----------------------
支持，可以在IOS应用商店搜索easy wifi config进行下载

请问为何BW16开发板配置联网老是联网失败？我这边账号密码明明是对的
---------------------------
检查一下供电情况，供电不足将导致联网超时

请问BW16模组可以连接车机完成wifi连接么？
-----------------------------
不能，BW16非网卡类模组

BW16模块为什么网络调试助手断开连接会返回socketAutodel，而模块断开连接不会自己返回
-------------------
因为模块断开连接属于手动删除链接，不符合自动删除链接的条件，所以不会返回socketAutodel

请问BW16模组能否做热点实现中继功能？
-------------------------
可以自行配置实现；

请问我这边用MCU驱动BW16模组，串口应该接的是LOG_TX、LOG_RX还是接AT_TX、AT_RX？
---------------------
接AT_TX跟AT_RX；

请问为何BW16模组启动配置串口波特率为115200出来的是乱码？
-------------
AT口的波特率要配置38400；

请问BW16的蓝牙主机模式下支持连接多个设备么？
-------------------
不支持，仅支持点对点；

请问BW16支持SPI接口实现AT指令交互功能的么？
----------------
目前没有，若有定制需求，请联系安信可工作人员；

请问BW16作为AP可以接入多少个设备呢？
-----------------
5个

请问BW16支持串口实现wifi透传功能么？
----------------
支持TCP、UDP透传功能；

请问BW16支持微信airkiss配网功能么？
-------------
BW16不支持；

请问BW16开发板的type-C口是否支持直接上传程序？
----------------
支持，该接口为烧录接口。

请问BW16由板载天线切换IPEX要如何操作？
---------------
通过改变电阻的位置实现。
.. image:: img/BW16dianzu.png

请问BW16在透传模式下支持一对多传输么？
------------
不支持，透传功能只能一对一。

请问BW16模组的复位引脚由其他单片机控制么？
-------------
是的。

请问BW16静态IP设置中掩码已经网关IP要如何配置呢？
---------------
先配置模组为动态配置IP模式，连接wifi以后读出网关IP以及掩码等相关内容并保存，配置静态IP的时候直接填写该IP以及掩码即可。

请问BW16模组需要手机与模组进行配对实现配网功能么？还是说一上电即可实现配网功能？
-----------------------
需要用户手动配置模组进入配网模式。

请问BW16支持flash大小256Mbits么？
-------------------
不支持，支持最大128Mbits

请问为何我BW16模组老是烧录失败？
---------------
检查一下接的串口是否正确，需要接Log_TX以及Log_RX用于烧录固件，另外要设置模组进入烧录模式。通过普通串口判断模组是否进入烧录模式（hex显示下移植打印“15”则成功进入烧录模式）。

请问BW16模组要那个引脚与MCU连接？
--------------
AT_RX以及AT_TX，开发板则接PB2_RX1以及PB1_TX1
