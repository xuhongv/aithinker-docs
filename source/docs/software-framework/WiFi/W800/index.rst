安信可 WB1系列模块 FAQ
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------


Ai-WB1-32S-CAM可以串口接4G模块吗
--------------
可以通过串口接4G模块

Ai-WB1-32S-CAM支持arduino开发吗
---------------------
目前暂不支持

Ai-WB1-32S-CAM上的摄像头是多少像素的
--------------------
摄像头是30W

Ai-WB1-32S-CAM摄像头开发板支持本地录像吗
------------
二次开发实现，目前现成固件仅支持拍照功能

Ai-WB1-32S-CAM可以实现拍摄视频然后保存到TF卡功能吗
---------------------
可以实现，需做开发

Ai-WB1-A1S-VOICE音频开发板有录音功能吗
-----------------
默认没有录音功能，需做开发实现