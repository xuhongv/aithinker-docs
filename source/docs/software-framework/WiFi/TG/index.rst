安信可 平头哥TG系列模块 FAQ
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------

TG-12F模块，只要在天猫精灵注册相关信息就可以直接连接实现天猫精灵控制么
--------------
是的

TG-12F需要自己烧录嘛？有直接连天猫精灵的嘛？
--------------
不需要您自己烧录，出厂的AT固件就能连接天猫精灵

TG-12F里面的默认固件控制有相关文档说明么？
--------------
https://aithinker.blog.csdn.net/article/details/109451425

TG-12F可以直接替换12S不？
--------------
可以替换

TG-12F-Kit开发板是不是用IDE开发
--------------
只能用liunx

TG-12F能直连天猫精灵的？
--------------
可以的，https://aithinker.blog.csdn.net/article/details/109451425

TG-12F开发板能接入小米吗？
--------------
目前暂不支持

TG-12F理想距离多远
--------------
300米

TG-12F模块支持二次开发吗？是不是阿里的license也在里面了
--------------
支持，https://aithinker.blog.csdn.net/article/details/110559410.在里面
TG-12F开发源码
github.com/Ai-Thinker-Open/Ai-Thinker-Open-TG7100C_SDK

TG-12F模块接天猫精灵，可以控制多少引脚呢？
--------------
可以控制13个引脚

TG-12F的蓝牙现在能独立使用吗
--------------
不能独立使用

TG-12F开发板可以同时接入天猫精灵和app吗
--------------
可以接入天猫精灵和app

TG-12F能监控用不
--------------
可以的

TG-12F开发板的开发资料
--------------
https://blog.csdn.net/Boantong_/article/details/110559410


TG-12F 开发板固件接入iot平台还需要自己开发吗
--------------
AT固件可以对接，连接TCP服务器；

TG-12F开发板对接阿里云iot平台需要烧录其他的软件吗
--------------
不需要哦，出厂AT固件就支持连接

TG-12F开发板上电在调试时发现有个问题 用AT指令发送任和数据 返回的都是OK，是怎么回事
--------------
使用时候需要将IO8拉低，拉低后再重新上电才能正常使用


TG-12F模组可以跑micropython程序吗
--------------------------------
不可以，目前没有适配的程序

TG-02模组支持arduino开发吗
--------------------------------
不支持

TG-01M模组的GND引脚是怎么对应的？
-------------------------------
如图：

.. image:: TG-pin.png


TG-02模组能否实现自组网？
----------------------
可以，需要二次开发，可在我们官网上找到开发相关资料。

TG-02模块可以通过AT指令修改蓝牙名称吗
--------------------------
不可以修改

请问一下TG-12F模组如何实现天猫精灵对接？
---------------------------
跟着说明文档（https://blog.csdn.net/Boantong_/article/details/109451425?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522167272431116800182734936%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=167272431116800182734936&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-3-109451425-null-null.blog_rank_default&utm_term=TG&spm=1018.2226.3001.4450）操作即可实现；

请问TG-01M天猫精灵模组是否支持二次开发？
-------------------------
支持的，`查看文档  <https://blog.csdn.net/Boantong_/article/details/110559410?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522167564622616800182130897%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=167564622616800182130897&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_ecpm_v1~rank_v31_ecpm-2-110559410-null-null.blog_rank_default&utm_term=TG-12&spm=1018.2226.3001.4450>`__

TG-01M开发板默认出货带阿里生活物联的激活码吗
----------------------
出货不带激活码，如果有该需求，可以联系商务进行定制

TG-12F模组可以在小米网关使用吗
------------------------
不可以，目前不支持对接小米网关

请问TG-12F模组是否自带固件，免配置，直接贴到底板供电就可以使用？
------------------
TG-12F模组自带AT固件，需要用户通过串口配置AT指令的方式实现入网需求。
