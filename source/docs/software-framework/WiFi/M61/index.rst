安信可 Ai-M61系列模块 FAQ
======

.. raw:: html

   <style>
   body {counter-reset: h2}
     h2 {counter-reset: h3}
     h2:before {counter-increment: h2; content: counter(h2) ". "}
     h3:before {counter-increment: h3; content: counter(h2) "." counter(h3) ". "}
     h2.nocount:before, h3.nocount:before, { content: ""; counter-increment: none }
   </style>

--------------
Ai-M61系列模组支持AD转换吗
--------------
模组支持AD转换，需通过开发实现

有使用网口链接的模块吗
---------------------
可以选择M61/62系列的模组

M61/62系列模组支持DAC功能吗，具体是使用哪个引脚
------------
模组支持DAC功能，channelA为GPIO3,channelB为GPIO2引脚

请问咱们wifi6模组图像数据实测传输速率多大？
---------------
可达50Mbps