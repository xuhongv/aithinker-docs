
***********
软件使用
***********
| 这里汇集了我司模组使用开发的常见问题

WiFi系列
---------
.. toctree::
   :maxdepth: 1
   :glob:

   WiFi/*

蓝牙系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   bt/*


2.4G系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   2.4G/*


GPRS系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   GPRS/*


GPS系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   GPS/*


UWB系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   UWB/*


LoRa/LoRaWan系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   LoRa-LoRaWan/*

4G Cat.1系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   4G Cat.1/*



离线语音系列
---------

.. toctree::
   :maxdepth: 1
   :glob:

   VC/*



NB系列
---------
.. toctree::
   :maxdepth: 1
   :glob:

   NB/*


雷达模组系列
---------
.. toctree::
   :maxdepth: 1
   :glob:

   Rd/*

   
开源硬件-小安派系列
---------
.. toctree::
   :maxdepth: 1
   :glob:

   Ai-pi/*

其它
---------
.. toctree::
   :maxdepth: 1
   :glob:

   TOOL/*