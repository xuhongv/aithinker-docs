Ai-M62系列
==================


1.1 Ai-M62系列出厂AT固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 固件版本号：(V0.0.1)
- 启动方式：自定义
- 固件大小：1038KB
- 日志串口|波特率：UART1(IO26、28) | 115200
- 命令串口|波特率：UART0(IO21、IO22) | 115200
- 支持升级：否
- MD5校验码：7430a8c1cb817a57cd5a84905ae22756


**启动信息**

::

 ################################################

 arch:BL616,NULL
 company:Ai-Thinker|B&T
 wifi_mac:b40ecf2a9a47
 sdk_version:release/DEBUG
 firmware_version:release/V4.18_P0.0.1
 compile_time:Apr 13 2023 20:17:44

 ready

 ################################################

**固件下载：** `(固件号：2127)  <https://docs.ai-thinker.com/_media/m62_combo_v0.01_01141118_1_.zip>`__

