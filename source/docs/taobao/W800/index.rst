Ai-WB1系列
==================

1.1  出厂AT固件
~~~~~~~~~~~~~~~~

**说明：**

- Ai-WB1系列出厂固件

**固件下载：**  `(点击下载)  Ai-WB1系列  <https://docs.ai-thinker.com/_media/ai-wb1/ai-wb1.zip>`__


1.2 LVGL V8.3 (ST7796 电阻触摸屏)Demo 固件
~~~~~~~~~~~~~~~~~~~

**说明:**

- Ai-WB1系列驱动4.0寸电阻触摸屏运行LVGL v8.3 Demo固件

**固件下载：**  `(点击下载)  <https://docs.ai-thinker.com/_media/ai-wb1/ai-wb1_lvgl.zip>`__



1.3 风扇控制固件
~~~~~~~~~~~~~~~~~~~

**说明：**

- Ai-WB1-A1S实现离线语音+APP+天猫精灵控制风扇

**固件下载：**  `(点击下载)  <https://docs.ai-thinker.com/_media/ai-wb1/ai-wb1-a1s-voice_v1.1.zip>`__


1.4 Ai-WB1-A1S-VOICE出厂固件
~~~~~~~~~~~~~~~~~~~

**说明：**

- Ai-WB1-A1S-VOICE出厂固件

-版本号V4.18_P1.2.0

**固件下载：**  `(点击下载)  <https://docs.ai-thinker.com/_media/ai-wb1-a1s-voice%E5%87%BA%E5%8E%82%E5%9B%BA%E4%BB%B6_v4.18_p1.2.0.rar>`__


1.5 Ai-WB1-A1S-ChatGPT-语音-常规固件
~~~~~~~~~~~~~~~~~~~
   
   **说明：**

   1. 固件版本号：(1.0.0)
   2. 启动方式：自定义
   3. 固件大小：2MB
   4. 日志串口|波特率：UART0(IO19、IO20) | 115200
   5. 命令串口|波特率：UART0(IO19、IO20) | 115200
   6. 支持升级：否
   7. 功能描述：W800_ChatGPT语音版常规固件

   - 启动信息

   ::

    ChatGPT Voice

    Sequence  Command             Description
    ------------------------------------------------------------------------------------
    1         t-baidu_AI          Baidu AI configuration information, parameter 1 language type, 0 is Chinese, 1 is English; example: t-baidu_AI("1","APP_CUID","API_KEY","SECRET_KEY")
    2         t-ChatGPT           ChatGPT key configuration information; example: t-ChatGPT("OPENAI_KEY")
    3         t-wifi              Test connecting with apsta mode; example: t-wifi("aithinker","aithinker")
    4         factory             Factory reset
    5         reset               Reset System
    6         demohelp            Display Help information
    ------------------------------------------------------------------------------------
    ssid:aithinker
    password=aithinker
    please wait connect net......


**固件下载：** `(点击下载)  <https://docs.ai-thinker.com/_media/w800-chatgpt-voice-commonfirmware_v4.18_p1.0.0.zip>`__


