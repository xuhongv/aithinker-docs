雷达系列
==================



1.1 Rd-01出厂固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 版本号 v4.18_p1.0.0




**固件下载：** `固件料号：(2088) <https://docs.ai-thinker.com/_media/2088_rd-01_v4.18_p1.0.0.zip>`__




1.2 Rd-03出厂固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 版本号 v1.5.4




**固件下载：** `固件料号：(2165) <https://docs.ai-thinker.com/_media/2165_xend101_mms01_burn_v1.5.4.zip>`__


1.3 Rd-03D出厂固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 版本号 V1.0.0




**固件下载：** `固件料号：(2307) <https://docs.ai-thinker.com/_media/rd-03d_firmware_v1.0.0.rar>`__



1.5 Rd-03E出厂固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 版本号 V1.0.0

**精准测距固件下载：** `固件料号：(2268) <https://docs.ai-thinker.com/_media/rd-03e%E7%B2%BE%E5%87%86%E6%B5%8B%E8%B7%9D%E5%87%BA%E5%8E%82%E5%9B%BA%E4%BB%B6.rar>`__

**手势识别固件下载：** `固件料号：(2269) <https://docs.ai-thinker.com/_media/rd-03e%E6%89%8B%E5%8A%BF%E8%AF%86%E5%88%AB%E5%87%BA%E5%8E%82%E5%9B%BA%E4%BB%B6.rar>`__
