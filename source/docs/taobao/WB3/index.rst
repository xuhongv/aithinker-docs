Ai-WB3系列
==================


1.1  Ai-WB3系列出厂AT固件
~~~~~~~~~~~~~~~~~~

**说明：**

- 固件版本号：(V4.18_P1.0.0)
- 启动方式：自定义
- 固件大小：2MB
- 日志串口|波特率：UART1(B8、-) | 921600
- 命令串口|波特率：UART0(A2、A3) | 115200
- 支持升级：否
- MD5校验码： a4faa938e41095c9e79d6d8dafa3acbd


**启动信息**

::

  ################################################
  arch:CHIP_LN882H,0x00000001
  company:Ai-Thinker|B&T
  wifi_mac:94C9600E2B17
  sdk_version:v1.7
  firmware_version:V4.18_P1.0.0
  compile_time:Mar 31 2023 17:44:01
  ################################################

  ready

**固件下载：** `(固件号：2106)  <https://docs.ai-thinker.com/_media/ai-wb3_v4.18_p1.0.0.zip>`__

