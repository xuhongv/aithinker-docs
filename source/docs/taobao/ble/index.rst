1. 奉加PB系列
==================

1.1 PB-01/PB-02 出厂AT固件
~~~~~~~~~~~~~~~~

**说明：**

- BLE常规功能
- Combo-AT固件

**固件下载：** `(固件号：1212)  <https://docs.ai-thinker.com/_media/comat_1212.zip>`__

**蓝牙ble透传(非mesh) AT指令集：**  `(指令集链接) <https://docs.ai-thinker.com/_media/pb%E7%B3%BB%E5%88%97%E6%A8%A1%E7%BB%84%E9%80%9A%E7%94%A8at_v1006%E7%89%88%E6%9C%AC%E6%8C%87%E4%BB%A4_v1.1.3.pdf>`__

1.2 天猫精灵对接AT专有固件
~~~~~~~~~~~~~~~~~~~

**说明:**

- PB-01/02 天猫精灵对接AT专有固件

**固件下载：** `(固件号：1261) <https://docs.ai-thinker.com/_media/alimeshat_1261.rar>`__

**天猫精灵对接AT指令集：** `(指令集链接) <https://docs.ai-thinker.com/_media/pb%E7%B3%BB%E5%88%97%E6%A8%A1%E5%9D%97_alimesh_at%E6%8C%87%E4%BB%A4%E6%89%8B%E5%86%8Cv0.1.pdf>`__

~~~~~~~~~~~~~~~~

1.3 PB-03系列模组Combo-AT固件
~~~~~~~~~~~~~~~~~~~

**说明:**

- Combo-AT固件


**固件下载：** `(固件号1589) <https://docs.ai-thinker.com/_media/combofirmware_v1.3.0.zip>`__


1.4 PB-03蓝牙从机连接状态保持浅休眠功耗版本AT固件
~~~~~~~~~~~~~~~~~~~

**说明:**

- 指令集同Combo-AT指令集

- 支持BLE从机连接状态下保持低功耗状态

- 若需要更低的平均功耗，适当调整从机连接间隔

**固件下载：** `(下载链接) <https://docs.ai-thinker.com/_media/%E8%93%9D%E7%89%99%E4%BB%8E%E6%9C%BA%E8%BF%9E%E6%8E%A5%E7%8A%B6%E6%80%81%E4%BF%9D%E6%8C%81%E6%B5%85%E4%BC%91%E7%9C%A0%E5%8A%9F%E8%80%97%E7%89%88%E6%9C%ACat%E5%9B%BA%E4%BB%B6.rar>`__


2. 泰凌TB系列
================


2.1 Ble AT固件
~~~~~~~~~~~~~~~~~~

**说明：**

- TB-02/03F/04模组Combo-AT固件
- 版本:V0.9

**固件下载：** `(固件号：1268) <https://docs.ai-thinker.com/_media/1268%E5%8F%B7%E5%9B%BA%E4%BB%B6%E5%8D%87%E7%BA%A7.zip>`__

2.2 私有mesh_AT固件
~~~~~~~~~~~~~~~~~~~~~~~

**说明：**

- TB-02/03F/04模组 私有mesh_AT固件
- 版本:V0.5

**固件下载：** `(固件号：1183) <https://docs.ai-thinker.com/_media/privatmesh_1183_.zip>`__


2.3 Combo-AT固件
~~~~~~~~~~~~~~~~~~

**说明：**

- TB-02/03F/04模组Combo-AT固件
- 运行速率更换为32M适配精度要求高的蓝牙连接
- 适配对应波特率 9600/115200/921600

**固件下载：** `下载链接 <https://axk.coding.net/s/e2ca04f3-9c52-4258-9736-df7ef38f879a>`__
